import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { animate, state, style, transition, trigger } from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
tab1:boolean=true;
tab2:boolean=false;
tab3:boolean=false;
  constructor(private translate:TranslateService){
    translate.setDefaultLang('en');
  }
  switchLang(event:any){
    this.translate.use(event.value);
  }


 
}
